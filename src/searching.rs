/// Binary Search
/// 
/// Returns whether or not a given list **list** contains an item **item**.
/// 
/// ```rust
/// let result = algorithms::searching::binary_search(vec![1,2,3], 4);
/// assert_eq!(result, false);
/// 
/// let result2 = algorithms::searching::binary_search(vec![1,2,3], 2);
/// assert_eq!(result2, true);
/// ```
pub fn binary_search(list: Vec<i32>, item: i32) -> bool {
    let mut low = 0;
    let mut mid;
    let mut high = list.len() - 1;

    while low <= high {
        mid = low + high;
        let element = list[mid];
        if element == item {
            return true;
        }
        if element > item {
            high = mid - 1;
        }
        else {
            low = mid + 1;
        }
    }

    false
}