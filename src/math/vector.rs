use std::ops::Add;
use std::ops::Sub;
use std::ops::Mul;
use std::ops::Div;


#[derive(Debug, Clone, Copy, PartialOrd, PartialEq)]
pub struct Vector2D<T> {
    pub x: T,
    pub y: T
}

impl <T> Vector2D<T> {
}

impl <T: Add<Output=T>> Add for Vector2D<T> {
    type Output = Vector2D<T>;

    fn add(self, _rhs: Vector2D<T>) -> Vector2D<T> {
        Vector2D {
            x: self.x + _rhs.x,
            y: self.y + _rhs.y
        }
    }
}

impl <T: Sub<Output=T>> Sub for Vector2D<T> {
    type Output = Vector2D<T>;

    fn sub(self, _rhs: Vector2D<T>) -> Vector2D<T> {
        Vector2D {
            x: self.x - _rhs.x,
            y: self.y - _rhs.y
        }
    }
}

impl <T: Mul<Output=T>> Mul for Vector2D<T> {
    type Output = Vector2D<T>;

    fn mul(self, _rhs: Vector2D<T>) -> Vector2D<T> {
        Vector2D {
            x: self.x * _rhs.x,
            y: self.y * _rhs.y
        }
    }
}

impl <T: Div<Output=T>> Div for Vector2D<T> {
    type Output = Vector2D<T>;

    fn div(self, _rhs: Vector2D<T>) -> Vector2D<T> {
        Vector2D {
            x: self.x / _rhs.x,
            y: self.y / _rhs.y
        }
    }
}

#[derive(Debug, Clone, Copy, PartialOrd, PartialEq)]
pub struct Vector3D<T> {
    pub x: T,
    pub y: T,
    pub z: T
}

impl <T> Vector3D<T> {
}

impl <T: Add<Output=T>> Add for Vector3D<T> {
    type Output = Vector3D<T>;

    fn add(self, _rhs: Vector3D<T>) -> Vector3D<T> {
        Vector3D {
            x: self.x + _rhs.x,
            y: self.y + _rhs.y,
            z: self.z + _rhs.z
        }
    }
}

impl <T: Sub<Output=T>> Sub for Vector3D<T> {
    type Output = Vector3D<T>;

    fn sub(self, _rhs: Vector3D<T>) -> Vector3D<T> {
        Vector3D {
            x: self.x - _rhs.x,
            y: self.y - _rhs.y,
            z: self.z - _rhs.z
        }
    }
}

impl <T: Mul<Output=T>> Mul for Vector3D<T> {
    type Output = Vector3D<T>;

    fn mul(self, _rhs: Vector3D<T>) -> Vector3D<T> {
        Vector3D {
            x: self.x * _rhs.x,
            y: self.y * _rhs.y,
            z: self.z * _rhs.z
        }
    }
}

impl <T: Div<Output=T>> Div for Vector3D<T> {
    type Output = Vector3D<T>;

    fn div(self, _rhs: Vector3D<T>) -> Vector3D<T> {
        Vector3D {
            x: self.x / _rhs.x,
            y: self.y / _rhs.y,
            z: self.z / _rhs.z
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vector2d_addition() {
        let v1 = Vector2D { x: 1, y: 2 };
        let v2 = Vector2D { x: -1, y: 2 };
        let v3 = Vector2D { x: 0, y: 4 };
        assert_eq!(v3, v1 + v2);
    }

    #[test]
    fn test_vector2d_subtraction() {
        let v1 = Vector2D { x: 1, y: 2 };
        let v2 = Vector2D { x: -1, y: 2 };
        let v3 = Vector2D { x: 2, y: 0 };
        assert_eq!(v3, v1 - v2);
    }

    #[test]
    fn test_vector2d_multiplication() {
        let v1 = Vector2D { x: 1, y: 2 };
        let v2 = Vector2D { x: -1, y: 2 };
        let v3 = Vector2D { x: -1, y: 4 };
        assert_eq!(v3, v1 * v2);
    }

    #[test]
    fn test_vector2d_division() {
        let v1 = Vector2D { x: 1, y: 2 };
        let v2 = Vector2D { x: -1, y: 2 };
        let v3 = Vector2D { x: -1, y: 1 };
        assert_eq!(v3, v1 / v2);
    }

    #[test]
    fn test_vector3d_addition() {
        let v1 = Vector3D { x: 1, y: 2, z: 3};
        let v2 = Vector3D { x: -1, y: 2, z: 4 };
        let v3 = Vector3D { x: 0, y: 4, z: 7};
        assert_eq!(v3, v1 + v2);
    }

    #[test]
    fn test_vector3d_subtraction() {
        let v1 = Vector3D { x: 1, y: 2, z: 3 };
        let v2 = Vector3D { x: -1, y: 2, z: 4 };
        let v3 = Vector3D { x: 2, y: 0, z: -1 };
        assert_eq!(v3, v1 - v2);
    }

    #[test]
    fn test_vector3d_multiplication() {
        let v1 = Vector3D { x: 1, y: 2, z: 3 };
        let v2 = Vector3D { x: -1, y: 2, z: 4 };
        let v3 = Vector3D { x: -1, y: 4, z: 12 };
        assert_eq!(v3, v1 * v2);
    }

    #[test]
    fn test_vector3d_division() {
        let v1 = Vector3D { x: 1, y: 2, z: 3 };
        let v2 = Vector3D { x: -1, y: 2, z: 4 };
        let v3 = Vector3D { x: -1, y: 1, z: 0 };
        assert_eq!(v3, v1 / v2);
    }
}