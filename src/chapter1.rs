pub mod algo_1 {
    #[allow(dead_code)]
    pub fn merge_sort(n: f64) -> f64 {
        return 64_f64 * n * n.log2();
    }

    #[allow(dead_code)]
    pub fn insertion_sort(n: f64) -> f64 {
        return 8_f64 * n.powi(2);
    }

    #[allow(dead_code)]
    pub fn compute_factorial(n: f64) -> f64 {
        if n == 1_f64 {
            return 1_f64;
        }

        return n * compute_factorial(n - 1_f64);

    }

    #[allow(dead_code)]
    pub fn ex_one_two_two() {
        for number in 1..100 {
            let float_num = number as f64;
            let ins_result = insertion_sort(float_num);
            let merge_result = merge_sort(float_num);
            if ins_result < merge_result {
                println!("Insertion sort result was faster by factor {}", ins_result/merge_result)
            } else {
                println!("Merge sort result was faster by factor {}", ins_result/merge_result)
            }
        }
    }

    #[allow(dead_code)]
    pub fn ex_one_two_three() {
        for number in 1..1000 {
            let result_one = (100 * (number * number)) as f64;
            let result_two = 2_f64.powi(number);
            if result_one < result_two {
                println!("Smaller: {}", number);
            }
        }
    }

    #[allow(dead_code)]
    pub fn problem_one_one() {
        for number in 100000..500000000 {
            let number_f = number as f64;
//            let alg = number_f.log2();
//            let alg = number_f.sqrt();
//            let alg = number_f;
            let alg = number_f * number_f.log2();
//            let alg = number_f.powi(2);
//            let alg = number_f.powi(3);
//            let alg = 2_f64.powf(number_f);
//            let alg = compute_factorial(number_f);

            let seconds = 10e6;
            let minutes = seconds * 60_f64;
            let hours = minutes * 60_f64;
            let days = hours * 24_f64;
            let months = days * 30_f64;
            let years = months * 12_f64;
            let century = years * 100_f64;

            if alg > century {
                println!("found: {}", number);
                return
            }
        }
    }
}