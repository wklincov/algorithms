use std::cmp;

#[allow(dead_code)]
pub fn insertion_sort(xs: &mut [i32]) {
    for j in 2..xs.len() + 1 {
        let key = xs[j - 1];
        let mut i = j - 1;
        while i > 0 && xs[i - 1] > key {
            xs[i] = xs[i - 1];
            i = i - 1;
        }
        xs[i] = key;
    }
}

#[allow(dead_code)]
pub fn reverse_insertion_sort(xs: &mut [i32]) {
    for j in (0..xs.len() - 1).rev() {
        let key = xs[j];
        let mut i = j + 1;
        while i < xs.len() && xs[i] > key {
            xs[i - 1] = xs[i];
            i = i + 1;
        }
        xs[i - 1] = key;
    }
}

#[allow(dead_code)]
pub fn binary_addition(xs: &Vec<i32>, ys: &Vec<i32>, zs: &mut Vec<i32>) {
    let max_length = cmp::max(xs.len(), ys.len());
    zs.resize(max_length + 1, 0);

    let mut carry_over = 0;

    for i in 0..max_length {
        let mut x = 0;
        if i < xs.len() {
            x = xs[xs.len() - 1 - i];
        }

        let mut y = 0;
        if i < ys.len() {
            y = ys[ys.len() - 1 - i];
        }

        let mut z: i32 = x + y + carry_over;

        if z == 2 {
            carry_over = 1;
            z = 0;
        } else if z == 1 {
            carry_over = 0;
        } else if z == 0 {
            carry_over = 0;
        }
        zs[max_length - i] = z;
    }

    // If we still have a carry over, we want to set the largest bit to 1
    if carry_over == 1 {
        zs[0] = 1;
    }
}

#[allow(dead_code)]
pub fn bubble_sort(xs: &Vec<i32>) -> Vec<i32> {
    let mut ys = xs.to_vec();
    for _ in 0..(xs.len()) {
        for j in (1..xs.len()).rev() {
            if ys[j] < ys[j - 1] {
                let tmp = ys[j];
                ys[j] = ys[j - 1];
                ys[j - 1] = tmp;
            }
        }
    }
    ys
}

pub fn selection_sort(xs: &Vec<i32>) -> Vec<i32> {
    let mut xss = xs.to_vec();
    let len_xs = xs.len();
    let mut ys = Vec::new();
    ys.resize(len_xs, 0);

    for i in 0..(xss.len()) {
        let mut smallest_element = xss[0];
        let mut smallest_index = 0;

        for j in 1..(xss.len()) {
            if xss[j] < smallest_element {
                smallest_element = xss[j];
                smallest_index = j;
            }
        }

        ys[i] = xss[smallest_index];
        xss.remove(smallest_index);
    }
    ys
}

/// Sort a vector:
/// ```
/// use algorithms::sorting::merge_sort;
/// let xs = vec![9, 4, 3];
/// let sorted_xz = merge_sort(&xs);
/// ```
#[allow(dead_code)]
pub fn merge_sort(xs: &Vec<i32>) -> Vec<i32> {
    let mut zs = xs.to_vec();
    split_merge(&mut zs, 0, xs.len());
    return zs;
}

fn split_merge(zs: &mut Vec<i32>, p: usize, r: usize) {
    if r - p < 2 {
        return;
    }

    let q = (p + r) / 2;
    split_merge(zs, p, q);
    split_merge(zs, q, r);
    merge(zs, p, q, r);
}

fn merge(zs: &mut Vec<i32>, p: usize, q: usize, r: usize) {
    let len_pq = q - p;
    let len_qr = r - q;

    let mut left_pq = Vec::new();
    left_pq.resize(len_pq + 1, 0);
    let mut right_qr = Vec::new();
    right_qr.resize(len_qr + 1, 0);

    for i in 0..(len_pq) {
        left_pq[i] = zs[p + i];
    }

    for j in 0..(len_qr) {
        right_qr[j] = zs[q + j];
    }

    left_pq[len_pq] = i32::max_value();
    right_qr[len_qr] = i32::max_value();

    let mut n = 0;
    let mut o = 0;

    for k in p..r {
        if left_pq[n] <= right_qr[o] {
            zs[k] = left_pq[n];
            n = n + 1;
        } else {
            zs[k] = right_qr[o];
            o = o + 1;
        }
    }
}

pub fn quicksort(list: &Vec<i32>) -> Vec<i32> {
    let mut list_copy = list.to_vec();
    let list_length = list_copy.len();

    if list_length < 2 {
        return list_copy;
    }

    let pivot = list_copy.remove(list_length / 2);

    let mut smaller_pivot = Vec::with_capacity(list_length);
    let mut bigger_pivot = Vec::with_capacity(list_length);

    for element in list_copy {
        if element < pivot {
            smaller_pivot.push(element);
        } else {
            bigger_pivot.push(element);
        }
    }

    let mut result = quicksort(&smaller_pivot);
    result.push(pivot);
    result.append(&mut quicksort(&bigger_pivot));
    return result;
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_insertion_sort() {
        let mut xs: [i32; 6] = [5, 2, 4, 6, 1, 3];
        insertion_sort(&mut xs);
        assert_eq!(xs, [1, 2, 3, 4, 5, 6]);
    }

    #[test]
    fn test_reverse_insertion_sort() {
        let mut xs: [i32; 6] = [5, 2, 4, 6, 1, 3];
        reverse_insertion_sort(&mut xs);
        assert_eq!(xs, [6, 5, 4, 3, 2, 1]);
    }

    #[test]
    fn test_binary_addition() {
        let xs = vec![1, 0, 1, 0, 1, 1];
        let ys = vec![1, 0, 0, 0, 1];
        let mut zs = Vec::new();
        binary_addition(&xs, &ys, &mut zs);
        assert_eq!(&zs[..], &[0, 1, 1, 1, 1, 0, 0]);

        let xs2 = vec![1];
        let ys2 = vec![1];
        let mut zs2 = Vec::new();
        binary_addition(&xs2, &ys2, &mut zs2);
        assert_eq!(&zs2[..], &[1, 0]);
    }

    #[test]
    fn test_merge_sort() {
        let sorted_zs = vec![2, 3, 4, 6, 7, 8, 8, 9, 9, 11, 13, 24, 24, 41, 47, 98];

        let xs = vec![9, 4, 3, 6, 7, 8, 2, 11, 24, 9, 13, 8, 24, 47, 98, 41];
        let zs = merge_sort(&xs);
        assert_eq!(&sorted_zs, &zs);
    }

    #[test]
    fn test_bubble_sort() {
        let sorted_zs = vec![2, 3, 4, 6, 7, 8, 8, 9, 9, 11, 13, 24, 24, 41, 47, 98];

        let xs = vec![9, 4, 3, 6, 7, 8, 2, 11, 24, 9, 13, 8, 24, 47, 98, 41];
        let zs = bubble_sort(&xs);
        assert_eq!(&sorted_zs, &zs);
    }

    #[test]
    fn test_selection_sort() {
        let sorted_zs = vec![2, 3, 4, 6, 7, 8, 8, 9, 9, 11, 13, 24, 24, 41, 47, 98];

        let xs = vec![9, 4, 3, 6, 7, 8, 2, 11, 24, 9, 13, 8, 24, 47, 98, 41];
        let zs = selection_sort(&xs);
        assert_eq!(&sorted_zs, &zs);
    }

    #[test]
    fn test_quicksort() {
        let sorted_zs = vec![2, 3, 4, 6, 7, 8, 8, 9, 9, 11, 13, 24, 24, 41, 47, 98];

        let xs = vec![9, 4, 3, 6, 7, 8, 2, 11, 24, 9, 13, 8, 24, 47, 98, 41];
        let zs = quicksort(&xs);
        assert_eq!(&sorted_zs, &zs);
    }

}
