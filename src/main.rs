mod chapter1;
mod sorting;


fn main() {
    let xs= vec![9, 5, 8, 2, 1, 3, 30, 7];
    println!("before: {:?}", xs);
    let zs = sorting::merge_sort(&xs);
    println!("after: {:?}", zs);
}